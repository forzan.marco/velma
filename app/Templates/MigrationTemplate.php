<?php
    namespace Velma\Templates;

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class MigrationTemplate extends Migration{
        
        /** 
        * Run the migrations.
        *
        * @return void
        */
        public function up()
        {
            Schema::create('migration_template', function (Blueprint $table) {
                $table->id();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('migration_template');
        }
    }
?>