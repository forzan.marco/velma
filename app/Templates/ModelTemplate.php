<?php
    namespace Velma\Templates;

    use Illuminate\Database\Eloquent\Model;

    class ModelTemplate extends Model{
        /**
        * Indicates if the model should be timestamped.
        * if false it removes created_at and updated_at.
        * @var bool
        */
        public $timestamps = false;
        public $fillable = [];
    }
?>