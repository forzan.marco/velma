<?php

namespace Velma\Providers;

use Illuminate\Support\ServiceProvider;
use Velma\Console\VelmaCommand;

class VelmaServiceProvider extends ServiceProvider 
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerVelmaCommand();

        $this->commands('velma.create');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(){

    }

    protected function registerVelmaCommand(){
        $this->app->singleton('velma.create', function () {
            return new VelmaCommand;
        });
    }
}
?>