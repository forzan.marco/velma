<?php
    namespace Velma\Templates;

    use Illuminate\Routing\Controller;
    use Illuminate\Http\Request;
    use App\Http\CustomModels\BaseResponseDto;
    use App\Services\ServiceTemplate;
    use Illuminate\Http\JsonResponse;


    class ControllerTemplate extends Controller{

        private ServiceTemplate $modelService;

        public function __construct(ServiceTemplate $modelService){
            $this->modelService = $modelService;
        }

        public function getAll(): JsonResponse{
            return new JsonResponse(new BaseResponseDto($this->modelService->getAll()));
        }

        public function getById(int $id): JsonResponse{
            if($this->modelService->existsById($id)){
                return new JsonResponse(new BaseResponseDto($this->modelService->getById($id)));
            } else {
                return new JsonResponse(new BaseResponseDto([], 404, "no record found with id " . $id));
            }
        }

        public function save(Request $request): JsonResponse{
            return new JsonResponse(new BaseResponseDto($this->modelService->create($request->all())));
        }

        public function update(Request $request, int $id): JsonResponse{
            if($this->modelService->update($request->all(), $id)){
                return new JsonResponse(new BaseResponseDto(true, 200,  "record with id " . $id . " updated")); 
            } else {
                return new JsonResponse(new BaseResponseDto(false, 404, "record not found"));  
            }
            
        }

        public function deleteById(int $id): JsonResponse{
            if($this->modelService->deleteById($id)){
                return new JsonResponse(new BaseResponseDto(true, 200,  "record with id " . $id . " deleted")); 
            } else {
                return new JsonResponse(new BaseResponseDto(false, 404, "record not found" )); 
            }
            
        }
    } 
?>