<?php

namespace Velma\Console;

use Illuminate\Console\Command;

class VelmaCommand extends Command{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'velma:create {name : the name of the entity you want to generate(singular case sensitive)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create an entity controller, service, model and migration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $entityName = $this->argument('name');
        $attributes = $this->addAttributes();
        $getByAttributes = false;
        $timestamps = $this->addTimestamps();

        if(!empty($attributes) || $timestamps){
            $getByAttributes = $this->addGetByAttributes();
        }
        
        $this->createModel($entityName, $attributes, $timestamps);
        $this->createMigration($entityName, $attributes, $timestamps);
        $this->createService($entityName, $getByAttributes);
        $this->createController($entityName, $getByAttributes);
        $this->updateApi($entityName, $getByAttributes);
        $this->createBaseResponse();
        return 0;
    }

    private function addAttributes(){
        $attributes = [];
        while($this->confirm('Do you wish to add attribute to your entity?')){
            $attributeName = $this->ask('What is the attribute name?');
            $attributeType = $this->choice(
                'Select the type of your attribute',
                ['integer', 'float', 'string', 'char', 'boolean', 'date', 'enum', 'uuid'],
                2,
                $maxAttempts = null,
                $allowMultipleSelections = false
            );
            $attribute = [$attributeName, $attributeType];
            switch($attributeType){
                case "char":
                case "string":
                    $attributeExtra = $this->ask('What is the length?');
                    array_push($attribute, $attributeExtra);
                    break;
                case "enum":
                    $attributeExtra = explode(',', $this->ask('Insert comma separated enum values without spaces'));
                    array_push($attribute, $attributeExtra);
                    break;
                case "float":
                    $attributeExtra = $this->ask('What is the float precision?');
                    array_push($attribute, $attributeExtra, 2);
            }
            array_push($attributes, $attribute);
        }
        return $attributes;
    }

    private function addTimestamps(){
        return $this->confirm('Do you wish to add timestaps: created_at and updayed_at?');
    }

    private function addGetByAttributes(){
        return $this->confirm('Do you wish to add a getBy method for other attributes?');
    }

    private function createModel($entityName, $attributes, $timestamps){
        $fillables = "";
        foreach($attributes as $attribute){
            $fillables .= "\"" . $attribute[0] . "\",";
        }
        $newFilePath = "app/Models/$entityName.php";
        $filecontent = file_get_contents('vendor/velma/velma/app/Templates/ModelTemplate.php');
        $filecontent = str_replace(['ModelTemplate', "Velma\Templates", "[]"], [$entityName, "App\Models", "[". rtrim($fillables, ",") . "]"], $filecontent);
        if($timestamps){
            $filecontent = str_replace('public $timestamps = false;', "", $filecontent);
        }
        file_put_contents($newFilePath, $filecontent);
    }

    private function createMigration($entityName, $attributes, $timestamps){
        $attributesString = '$table->id();'. "\n";
        foreach($attributes as $attribute){
            switch($attribute[1]){
                case "char":
                case "string":
                    $attributesString .= "\t\t\t\t".'$table->'.$attribute[1]."('".$attribute[0]."', ". $attribute[2].");\n";
                    break;
                case "enum":
                    $attributesString .= "\t\t\t\t".'$table->'.$attribute[1]."('".$attribute[0]."', [";
                    for($i = 0; $i < count($attribute[2]); $i++){
                        $attributesString .= "\"" . $attribute[2][$i] . "\"";
                        if($i < count($attribute[2]) - 1){
                            $attributesString .= ",";
                        }
                    }
                    $attributesString .= "]);\n";
                    break;
                case "float":
                    $attributesString .= "\t\t\t\t".'$table->'.$attribute[1]."('".$attribute[0]."', ". $attribute[2].", ". $attribute[3].");\n";
                    break;
                default:
                    $attributesString .= "\t\t\t\t".'$table->'.$attribute[1]."('".$attribute[0]."');\n";
            }   
        }
        if($timestamps){
            $attributesString .= "\t\t\t\t\$table->timestamp('created_at');\n\$table->timestamp('updated_at');";
        }
        $newFilePath = "database/migrations/". date("Y_m_d_His") . "_create_" . strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $entityName)) . "s_table.php";
        $filecontent = file_get_contents('vendor/velma/velma/app/Templates/MigrationTemplate.php');
        $filecontent = str_replace(['MigrationTemplate', '$table->id();', "namespace Velma\Templates;", "migration_template"],
                                   ["Create".$entityName."sTable", $attributesString, "", strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $entityName . "s"))],
                                      $filecontent);
        file_put_contents($newFilePath, $filecontent);
    }
    public function createService($entityName, $getByAttributes){
        $newFilePath = "app/Services/". $entityName ."Service.php";
        $filecontent = file_get_contents('vendor/velma/velma/app/Templates/ServiceTemplate.php');

        if($getByAttributes){
            $attributeMethod = '        /**
            * example of showing multiple possible return types
            * @return Collection|ModelTemplate could be a collection of the model, could be a model
            */
           public function getByAttributes(array $attributes, bool $first = false) {
               return $first? ModelTemplate::where($attributes)->first() : ModelTemplate::where($attributes)->get();
           } 
        }';
           $filecontent = substr_replace($filecontent, $attributeMethod, strrpos($filecontent, '}'), strlen('}'));
        }

        $filecontent = str_replace(['ServiceTemplate', "Velma\Templates", "ModelTemplate"],
                                   [ $entityName . "Service", "App\Services", $entityName],
                                    $filecontent);
        if (!file_exists("app/Services")) {
            mkdir("app/Services", 0755, true);
        }

        file_put_contents($newFilePath, $filecontent);
    }

    public function createBaseResponse(){
        if(file_exists("app/Http/CustomModels/BaseResponseDto.php")){
            return;
        }
        if(!file_exists("app/Http/CustomModels")){
            mkdir("app/Http/CustomModels", 0755, true);
        }
        file_put_contents("app/Http/CustomModels/BaseResponseDto.php", file_get_contents('vendor/velma/velma/app/Templates/BaseResponseDto.php'));

    }

    public function createController($entityName, $getByAttributes){
        $newFilePath = "app/Http/Controllers/". $entityName ."Controller.php";
        $filecontent = file_get_contents('vendor/velma/velma/app/Templates/ControllerTemplate.php');

        if($getByAttributes){
            $attributeMethod = '        public function getByAttributes(Request $request): JsonResponse{
                return new JsonResponse(new BaseResponseDto($this->modelService->getByAttributes($request->input(\'attributes\'), $request->input(\'first\'))));
            }   
        }';
            $filecontent = substr_replace($filecontent, $attributeMethod, strrpos($filecontent, '}'), strlen('}'));
        }


        $filecontent = str_replace(['ServiceTemplate', "modelService", 'ControllerTemplate', 'Velma\Templates'],
                                   [$entityName . "Service",lcfirst($entityName) . "Service", $entityName . "Controller", "App\Http\Controllers"],
                                    $filecontent);

        file_put_contents($newFilePath, $filecontent);
    }

    public function updateApi($entityName, $getByAttributes){
        //add import
        $file = file_get_contents('routes\api.php');
        $file = str_replace("<?php", "<?php\nuse App\\Http\\Controllers\\". $entityName ."Controller;", $file);

        //add routing
        if($getByAttributes){
            $file .= "\n\nRoute::prefix('". strtolower($entityName) . "')->group(function () {
                Route::get('/get', [". $entityName . "Controller::class, 'getAll']);
                Route::get('/get/{id}', [". $entityName . "Controller::class, 'getById']);
                Route::post('/save', [". $entityName . "Controller::class, 'save']);
                Route::put('/update/{id}', [". $entityName . "Controller::class, 'update']);
                Route::delete('/delete/{id}', [". $entityName . "Controller::class, 'deleteById']);
                Route::post('/getByAttributes', [". $entityName . "Controller::class, 'getByAttributes']);
    
            });";
        } else {
            $file .= "\n\nRoute::prefix('". strtolower($entityName) . "')->group(function () {
                Route::get('/get', [". $entityName . "Controller::class, 'getAll']);
                Route::get('/get/{id}', [". $entityName . "Controller::class, 'getById']);
                Route::post('/save', [". $entityName . "Controller::class, 'save']);
                Route::put('/update/{id}', [". $entityName . "Controller::class, 'update']);
                Route::delete('/delete/{id}', [". $entityName . "Controller::class, 'deleteById']);
    
            });";
        }

        file_put_contents('routes\api.php', $file);
    }
}

?>