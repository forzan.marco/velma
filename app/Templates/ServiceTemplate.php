<?php
    namespace Velma\Templates;

    use App\Models\ModelTemplate;
    use Illuminate\Database\Eloquent\Collection;

    class ServiceTemplate {
        
        public function existsById(int $id): bool{
            return ModelTemplate::where('id', $id)->exists();
        }

        public function getById(int $id): ModelTemplate{
            return ModelTemplate::find($id);
        }

        public function getAll(): Collection{
            return ModelTemplate::get();
        }

        public function create(array $model): ModelTemplate{
            return ModelTemplate::create($model);
        }

        public function update(array $model, int $id): bool{
            if($this->existsById($id)){
                $dbModel = ModelTemplate::find($id);
                foreach(array_keys($model) as $key){
                    $dbModel[$key] = $model[$key];
                }
                $dbModel->save();
                return true;
            } else {
                return false;
            }
        }

        public function deletebyId(int $id): bool {
            if($this->existsById($id)){
                ModelTemplate::destroy($id);
                return true;
            } else {
                return false;
            }
        }
    }
?>